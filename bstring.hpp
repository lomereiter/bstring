#ifndef __BSTRING_HPP__
#define __BSTRING_HPP__

#include <cstddef>
#include <algorithm>
#include <cassert>
#include <iostream>

class BString {
    char* data_;
    size_t length_;
  public:
    BString();
    BString(const char* str);
    BString(const BString& str);
    BString& operator=(const BString& other);
    size_t length() const;
    const char* data() const;
    char operator[](size_t index) const;
    char& operator[](size_t index);
    BString operator()(size_t from, size_t to) const;
    friend BString operator+(const BString& s1, const BString& s2);
    friend BString operator+(const BString& s1, char c);
    BString& operator+=(const BString& other);
    ~BString();
};

bool operator==(const BString& s1, const BString& s2);
BString operator+(const BString& s1, const BString& s2);
BString operator+(const BString& s1, char c);
std::ostream& operator<<(std::ostream& stream, const BString& s);

#endif
