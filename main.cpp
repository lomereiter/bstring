#include "bstring.hpp"

int main() {
    BString s1("abcd");
    BString s2("efgh");
    
    assert(s1[0] == 'a');
    assert(s1[3] == 'd');
    assert(s1.length() == 4);
    assert(s2.length() == 4);

    BString s3 = s1 + s2;
    assert(s3.length() == 8);
    assert(s3[5] == 'f');

    BString s4 = s3(2, 6);
    s4[2] = 'z';
    s4 += "xyz";

    s2 = s4;
    assert(s2 == "cdzfxyz");
}
