#include <cstddef>
#include <algorithm>
#include <cassert>
#include <iostream>

#include "bstring.hpp"

BString::BString() : data_(NULL), length_(0) {}

BString::BString(const char* str) {
    const char* p = str;
    while (*p++) {}
    size_t len = p - str - 1;
    data_ = new char[len];
    std::copy(str, str + len, data_);
    length_ = len;
}

BString::BString(const BString& str) {
    data_ = new char[str.length()];
    std::copy(str.data_, str.data_ + str.length(), data_);
    length_ = str.length_;
}

BString& BString::operator=(const BString& other) {
    delete[] data_;
    data_ = new char[other.length_];
    std::copy(other.data_, other.data_ + other.length_, data_);
    length_ = other.length_;
    return *this;
}

size_t BString::length() const {
    return length_;
}

const char* BString::data() const {
    return data_;
}

char BString::operator[](size_t index) const {
    return data_[index];
}

char& BString::operator[](size_t index) {
    return data_[index];
}

BString BString::operator()(size_t from, size_t to) const {
    BString s;
    s.data_ = new char[to - from];
    s.length_ = to - from;
    std::copy(data_ + from, data_ + to, s.data_);
    return s;
}

BString& BString::operator+=(const BString& other) {
    char* ptr = new char[length_ + other.length_];
    std::copy(data_, data_ + length_, ptr);
    std::copy(other.data_, other.data_ + other.length_, ptr + length_);
    length_ += other.length_;
    delete[] data_;
    data_ = ptr;
    return *this;
}

BString::~BString() {
    delete[] data_;
}

bool operator==(const BString& s1, const BString& s2) {
    if (s1.length() != s2.length())
        return false;
    size_t len = s1.length();
    for (size_t i = 0; i < len; ++i)
        if (s1[i] != s2[i])
            return false;
    return true;
}

BString operator+(const BString& s1, const BString& s2) {
    BString s;
    s.length_ = s1.length() + s2.length();
    s.data_ = new char[s1.length() + s2.length()];
    std::copy(s1.data_, s1.data_ + s1.length(), s.data_);
    std::copy(s2.data_, s2.data_ + s2.length(), s.data_ + s1.length());
    return s;
}

BString operator+(const BString& s1, char c) {
    BString s;
    s.length_ = s1.length() + 1;
    s.data_ = new char[s1.length() + 1];
    std::copy(s1.data_, s1.data_ + s1.length(), s.data_);
    s.data_[s.length_ - 1] = c;
    return s;
}

std::ostream& operator<<(std::ostream& stream, const BString& s) {
    size_t len = s.length();
    for (size_t i = 0; i < len; ++i)
        stream << s[i];
    return stream;
}
